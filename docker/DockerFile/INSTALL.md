# 0. Se supone que tenemos instalado docker en nuestro equipo, cada sistema operativo debe seguir su propio procedimiento.

# 1. Construir Imagen para correr haskell con Arch Linux (en el directorio que tengamos Dockerfile)

### docker build -t nukebull/haskelltalk .

# 2. Miramos IMAGE ID con el siguiente comando para crar el contenedor

### docker images 

# 3. Creamos el contenedor, usaremos el código haxadecimal en el campo IMAGE ID (podeis sustituir haskelltalk por otro nombre que os agrade más, pero acordaros de el en el paso 6)

### docker create --name haskelltalk -h harchy -i -t IMAGE ID  /bin/bash

# 4. Miramos CONTAINERE ID con el siguiente comando para arrancar/iniciar el contenedor

### docker ps -a

# 5. Iniciamos el Contenedor introduciendo el valor de CONTAINER ID

### docker start CONTAINER ID

# 6. Entramos en el Contenedor en modo consola con bash (haselltalk es el nombre elegido en el paso 3)

### docker exec -it haskelltalk bash



