import NukeBull (randomB,noOrder)
import System.Environment
import Data.Word
import Data.Bits
import System.Random


import qualified Data.ByteString as B
import qualified Data.ByteString.Base64 as BS



generator :: (RandomGen g ) => Int -> Int -> g ->  [Int]
generator n m g = take n $ randomRs (0,m) g

choose :: Int -> B.ByteString -> B.ByteString
choose n b = B.drop (n-1) (B.take n b )

ordercrazy :: Int -> [Word8] ->[Int] -> [Int] -> [Word8]
ordercrazy 1 c s1 s2 = noOrder c s1 s2
ordercrazy n c s1 s2 = noOrder ( ordercrazy (n-1) c s1 s2 )s1 s2 

crazyloop :: (RandomGen g) => Int -> Int -> g -> IO B.ByteString
crazyloop n m g = do
          poll <- randomB m
          g <- newStdGen
          let droping = generator n (m-1) g
          let password = B.concat ( map (\x -> B.drop x (B.take (x+1) poll)) droping )
          return password




main = do 
    (comando:_) <- getArgs



    case comando of 

        "lazy" -> do   
                (_:fileName10:_) <- getArgs                  
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                -- Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 RandomGen Lv 3.2              

                passwords <- randomB 128                  
                B.writeFile fileName1 passwords                    
                putStrLn "Creating  vectorIV -------------  OK"
                putStrLn "Creating  AES256 Key -----------  OK"
                putStrLn "Creating  Password Lv. 3.1 -----  OK"
                putStrLn "Creating  Password Lv. 3.2 -----  OK"
                putStrLn "Writing Password ---------------  OK"

        "crazy" -> do   
                (_:fileName10:_) <- getArgs                  
                let fileName1a = "Keys/" ++ fileName10 ++ "a.key"
                let fileName1b = "Keys/" ++ fileName10 ++ "b.key"
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"

                -- Read Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 RandomGen Lv 3.2              
                
                putStrLn "Creating Random Poll ---------------------------  OK"
                putStrLn "Please wait, Millions of bits Playing ----------  OK"              

                g <- getStdGen 

                password1 <- crazyloop 64 1048576 g
                putStr "[------25%"
                g <- newStdGen                
                password2 <- crazyloop 64 1048576 g
                putStr "------50%"
                g <- newStdGen                
                password3 <- crazyloop 64 1048576 g
                putStr "------75%"
                g <- newStdGen                
                password4 <- crazyloop 64 1048576 g
                putStr "------100%]"
                let passwords = B.unpack(password1) ++ B.unpack(password2) ++ B.unpack(password3) ++ B.unpack(password4)
                
                g <- newStdGen
                let rows = generator 32 15 g
                g <- newStdGen
                let cols = generator 32 15 g

                let password = B.take 128 (B.pack (ordercrazy 30 passwords rows cols))
                let passworda = B.drop 128 (B.pack (ordercrazy 30 passwords rows cols))
                let passwordb = B.pack (B.zipWith (xor) ( password) ( passworda))
                                              
                B.writeFile fileName1 password

                let base64a = BS.encode passworda
                let base64b = BS.encode passwordb

                B.writeFile fileName1a base64a
                B.writeFile fileName1b base64b

                putStrLn ""
                putStrLn ""
                putStrLn "================================================================="
                putStrLn "=====   N   N  U  U  K  K  EEEE     BBB   U  U  L    L      ====="
                putStrLn "=====   NN  N  U  U  K K   E        B  B  U  U  L    L      ====="
                putStrLn "=====   N N N  U  U  KK    EEE      BBB   U  U  L    L      ====="
                putStrLn "=====   N  NN  U  U  K K   E        B  B  U  U  L    L      ====="
                putStrLn "=====   N   N  UUUU  K  K  EEEE     BBB   UUUU  LLLL LLLL   ====="
                putStrLn "================================================================="
                putStrLn ""
                putStrLn ("     Your password has been created in " ++ (show(fileName1)))
                putStrLn (" Two Complementary keys in " ++ (show(fileName1a)) ++" and "++ (show(fileName1a)))
                putStrLn ""
                putStrLn ""

        "xor" -> do
                (_:fileName10:_) <- getArgs
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                let fileName1a = "Keys/" ++ fileName10 ++ "a.key"
                let fileName1b = "Keys/" ++ fileName10 ++ "b.key"
                
                base64a <- B.readFile fileName1a
                let keya = BS.decodeLenient base64a
                base64b <- B.readFile fileName1b
                let keyb = BS.decodeLenient base64b

                let password = B.pack (B.zipWith (xor) ( keya) (keyb))
                B.writeFile fileName1 password
                putStrLn ""
                putStrLn ""
                putStrLn "================================================================="
                putStrLn "=====   N   N  U  U  K  K  EEEE     BBB   U  U  L    L      ====="
                putStrLn "=====   NN  N  U  U  K K   E        B  B  U  U  L    L      ====="
                putStrLn "=====   N N N  U  U  KK    EEE      BBB   U  U  L    L      ====="
                putStrLn "=====   N  NN  U  U  K K   E        B  B  U  U  L    L      ====="
                putStrLn "=====   N   N  UUUU  K  K  EEEE     BBB   UUUU  LLLL LLLL   ====="
                putStrLn "================================================================="
                putStrLn ""
                putStrLn ("     Your password has been created in " ++ (show(fileName1)))
                putStrLn ""

        "info" -> do  
                putStrLn "stack exec KeyGen-exe [Argument]  FileName (Password) without extension"
                putStrLn "Arguments:"
                putStrLn "lazy  - It is a basic Random Numbers Gen       :: runhaskell KeyGen.hs lazy  FileName(Password)"
                putStrLn "crazy - It is the Paranoid Random Number Gen   :: runhaskell KeyGen.hs crazy FileName(Password)"
                putStrLn "xor   - Joins the two complementary passwords  :: runhaskell KeyGen.hs xor   FileName(Password)" 

        otherwise ->do
                putStrLn "Arguments Is Not Valid"
                putStrLn "Used: runhaskell KeyGen.hs lazy  FileName(Password)"
                putStrLn "Used: runhaskell KeyGen.hs crazy FileName(Password)"
                putStrLn "Used: runhaskell KeyGen.hs xor   FileName(Password)"

          
