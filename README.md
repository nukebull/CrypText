# CrypText alpha 0.3

## Encrypt -- plain text app  

This aplication is written in Haskell language.  

In this Alpha 0.3 used four layers for encrypt a 128 bytes text.  

## Layer 1 Entrophy (Anti frist-last Block Defending)  

Entrophy Add 128 Random bytes.  

EntrophyText = (Random 128 bytes) ++ Text XOR ( Random 128 bytes) = 256 bytes.  

## Layer 2 AES256 -- Cipher Block Chaining (CBC).

User 256 bits AES Password + 128 bits vectorIV (CBC).  
 

## Layer 3 Strong Cipher

Password Lv.3.1. Generates a matrix of 16 * 16 bytes   

16 bytes => 16 row translions + 16 cols traslations (hex code) 
Generate 16 * 16 transform Matrix, Change the position of 256 bytes in AES encriptation

Pasword Lv 3.2. 4 * 16 bytes => Generate 16 Randon Strong keys (256 bytes)
Use Colin Percival Stronger Key Derivation via Sequential Memory-Hard Functions
http://www.tarsnap.com/scrypt/scrypt.pdf

The cipher are 16 rounds (Marix 256 Translation + XOR KDF-Keys). 

Encrypt text 256 Bytes

## Layer 4 Encode base64

Base64 Encode for Humans. 

Encode Text 344 Bytes

# Instructions for Use.  

see INSTALL.MD

### Key generator:  

stack exec KeyGen-exe argument NameKey

Argument: lazy, crazy, xor, info

Example: stack exec KeyGen-exe crazy passOne

### Encrypt:

stack exec CrypText-exe encrypt FilePad1 FilePad2.  

Filepad1 = Passwords Name.  

FilePad2 = Output Encrypted File Name.  

Example: stack exec CrypText-exe encrypt passOne CODE  

### Decript:  

stack exec CrypText-exe encrypt FilePad1 FilePad2  

Filepad1 = Passwords Name.  

FilePad2 = Output Encrypted File Name.  

Example: stack exec CrypText-exe decrypt passOne CODE  

