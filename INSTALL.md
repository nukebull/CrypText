# INSTALL

## 1.1.a Install git.

https://git-scm.com/downloads

## 1.1.b Install repository.

git clone https://framagit.org/nukebull/cryptext.git

## 1.2 Alternative Download zip-tar File from:

https://framagit.org/nukebull/cryptext

## 2 Install stack for Haskell:

https://docs.haskellstack.org/en/stable/README/#how-to-install

#### OR

https://www.haskell.org/downloads/#stack

## 3. Build and compiling in the Directory CrypText.

stack build

## 4.a Generate First Passwords.

stack exec KeyGen-exe crazy FirstKey

## 4.b Encrypt First Text.

stack exec CrypText-exe encrypt FirstKey EncodeFile

## 4.c Decrypt First Text.

stack exec CrypText-exe decrypt FirstKey EncodeFile
